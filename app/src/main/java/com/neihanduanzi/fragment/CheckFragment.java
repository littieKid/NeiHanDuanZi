package com.neihanduanzi.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.neihanduanzi.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class CheckFragment extends BaseFragment {


    public CheckFragment() {
        // Required empty public constructor
    }

    @Override
    public String getFragmentTitle() {
        return "新鲜";
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_check, container, false);
    }

}
