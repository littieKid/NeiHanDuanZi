package com.neihanduanzi.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.gson.Gson;
import com.neihanduanzi.BaseActivity;
import com.neihanduanzi.R;
import com.neihanduanzi.model.ImageModel;
import com.neihanduanzi.myview.CircleImageView;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

/**
 * Created by Administrator on 2016/11/1 0001.
 */

public class ImageListAdapter extends RecyclerView.Adapter implements View.OnClickListener {
    private Context mContext;
    private List<ImageModel.Data> modelList;

    public ImageListAdapter(Context mContext, List<ImageModel.Data> modelList) {
        this.mContext = mContext;
        this.modelList = modelList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder ret = null;

        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.item_image, parent, false);
        view.setOnClickListener(this);
        ret = new MyViewHolder(view);
        return ret;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        MyViewHolder myViewHolder = (MyViewHolder) holder;
        myViewHolder.bindView(modelList.get(position), position);
    }

    @Override
    public void onClick(View v) {
        ImageModel.Data data = new ImageModel.Data();
        EventBus.getDefault().post(data);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private final CircleImageView userImage;
        private final TextView userName;
        private final TextView content;
        private final TextView shareFrom;
        private final ImageView image;
        private final CheckBox digg;
        private final CheckBox bury;
        private final CheckBox discussCount;
        private final CheckBox share;
        private ImageModel.Data group;

        public MyViewHolder(View itemView) {
            super(itemView);
            userImage = (CircleImageView)itemView.findViewById(R.id.user_image);
            userName = (TextView)itemView.findViewById(R.id.user_name);
            content = (TextView)itemView.findViewById(R.id.content_view);
            shareFrom = (TextView)itemView.findViewById(R.id.image_from);
            image = (ImageView)itemView.findViewById(R.id.image_view);
            digg = (CheckBox)itemView.findViewById(R.id.image_digg);
            bury = (CheckBox)itemView.findViewById(R.id.image_bury);
            discussCount = (CheckBox)itemView.findViewById(R.id.image_discuss_count);
            share = (CheckBox)itemView.findViewById(R.id.shar_view);
            itemView.setOnClickListener(this);
        }

        void bindView(final ImageModel.Data data, int position){
            group = data;
            if (data.getType() == 1){
                if (userImage != null){
                    String cover = data.getGroup().getUser().getAvatar_url();
                   if (cover != null){
                       Context context = userImage.getContext();
                       Picasso.with(context)
                               .load(cover)
                               .resize(320, 200)
                               .config(Bitmap.Config.ARGB_8888)
                               .into(userImage);
                   }
                }

                userName.setText(data.getGroup().getUser().getName());
                content.setText(data.getGroup().getContent());
                shareFrom.setText(data.getGroup().getCategory_name());

                if (image != null){
                    String url = data.getGroup().getMiddle_image().getUrl_list().get(0).getUrl();
                    if (url != null){
                        Context context = image.getContext();
                        Picasso.with(context)
                                .load(url)
                                .config(Bitmap.Config.ARGB_8888)
                                .into(image);
                    }

                if (data.getGroup().getIs_gif() == 1){
                        String urlGif = data.getGroup().getLarge_image().getUrl_list().get(0).getUrl();
                        if (urlGif != null){
                            Context context = image.getContext();
                            Glide.with(context)
                                    .load(urlGif)
                                    .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                                    .into(image);
                        }
                }
                }


                digg.setText(data.getGroup().getDigg_count() + "");
                bury.setText(data.getGroup().getBury_count() + "");
                discussCount.setText(data.getGroup().getComment_count() + "");
                share.setText(data.getGroup().getShare_count() + "");


            }
        }

        @Override
        public void onClick(View v) {
            if (group != null) {
                Gson gson = new Gson();
                String str = gson.toJson(group);
                Intent intent = new Intent(v.getContext(), BaseActivity.class);
                intent.putExtra("Data", str);
                intent.putExtra("type", 3);
                intent.putExtra("group_id", group.getGroup().getGroup_id());
                v.getContext().startActivity(intent);
            }
        }

//        @Override
//        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//            EventBus.getDefault().post(group);
//        }
    }

    @Override
    public int getItemCount() {
        int ret = 0;
        if (modelList != null){
            ret = modelList.size();
        }
        return ret;
    }
}
