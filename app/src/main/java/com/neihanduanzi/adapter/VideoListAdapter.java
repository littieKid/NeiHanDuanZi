package com.neihanduanzi.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.neihanduanzi.R;
import com.neihanduanzi.model.VideoModel;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;

import java.io.IOException;
import java.util.List;

/**
 * Created by 齐泽威 on 2016/10/31.
 */

public class VideoListAdapter extends BaseAdapter implements MediaPlayer.OnCompletionListener, MediaPlayer.OnPreparedListener {
    public static String mUrl;
    private static MediaPlayer sMediaPlayer;
    private static int sPlayingPosition;
    private List<VideoModel.Fdata> mFdatas;
    private Context mContext;
    private LayoutInflater mInflater;
    
    public VideoListAdapter(Context context, List<VideoModel.Fdata> fdatas) {
        mContext = context;
        mFdatas = fdatas;
        mInflater = LayoutInflater.from(context);
        if (sMediaPlayer == null) {
            sMediaPlayer = new MediaPlayer();
        } else {
            sMediaPlayer.release();
        }
        sPlayingPosition = -1;
        sMediaPlayer.setOnCompletionListener(this);
        sMediaPlayer.setOnPreparedListener(this);
    }

    @Override
    public int getCount() {
        return mFdatas.size();
    }

    @Override
    public Object getItem(int position) {
        return mFdatas.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.zvideofragment_item, parent, false);
            convertView.setTag(new ViewHolder(convertView));
        }
        ViewHolder holder = (ViewHolder) convertView.getTag();
        final VideoModel.Fdata fdata = mFdatas.get(position);
        if (fdata.getType() == 1) {
            holder.username.setText(fdata.getVideoDetial().getUser().getName());
            holder.videotitle.setText(fdata.getVideoDetial().getTitle());
            holder.videofenlei.setText(fdata.getVideoDetial().getCategory_name());
            holder.dianzan.setText(String.valueOf(fdata.getVideoDetial().getDigg_count()));
            holder.cai.setText(String.valueOf(fdata.getVideoDetial().getBury_count()));
            holder.reply.setText(String.valueOf(fdata.getVideoDetial().getRepin_count()));
            holder.sharecount.setText(String.valueOf(fdata.getVideoDetial().getShare_count()));
            Picasso.with(mContext).load(fdata.getVideoDetial().getUser().getIconUrl()).into(holder.usericon);
            Picasso.with(mContext).load(fdata.getVideoDetial().getCover().getUrl_list().get(0).getUrl())
                    .into(holder.cover);
            holder.cover.setVisibility(View.VISIBLE);
            holder.sharecount.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    EventBus.getDefault().post(fdata);
                }
            });

            mUrl = fdata.getVideoDetial().getMp4_url();

            if (mUrl != null) {
                holder.setItemPosition(position);
                holder.setInfo(mUrl);
            }
            
        }
        return convertView;
    }

   

    public void destroy() {
        if (sMediaPlayer != null) {
            sMediaPlayer.stop();
            sMediaPlayer.reset();
            sMediaPlayer.release();
            sMediaPlayer = null;
        }
    }

    public  class ViewHolder implements View.OnClickListener, SurfaceHolder.Callback, MediaPlayer.OnCompletionListener {
        private TextView username;
        private TextView videotitle;
        private TextView videofenlei;
        private CheckBox dianzan;
        private CheckBox cai;
        private CheckBox reply;
        private CheckBox sharecount;
        private ImageView usericon;
        private ImageView cover;
        private ImageView btnplay;
        private ImageView btnplay2;
        private SurfaceView mSurfaceView;
        private int itemPosition;
        private int mPosition;
        private String vplayUrl;


        public ViewHolder(View itemView) {
            username = ((TextView) itemView.findViewById(R.id.user_name));
            videofenlei = ((TextView) itemView.findViewById(R.id.video_fenlei));
            videotitle = ((TextView) itemView.findViewById(R.id.video_title));
            usericon = ((ImageView) itemView.findViewById(R.id.user_icon));
            sharecount = (CheckBox) itemView.findViewById(R.id.video_share_count);
            btnplay = (ImageView) itemView.findViewById(R.id.btn_play);
            btnplay2 = (ImageView) itemView.findViewById(R.id.btn_play2);
            dianzan = (CheckBox) itemView.findViewById(R.id.video_dianzan_count);
            cai = (CheckBox) itemView.findViewById(R.id.video_cai_count);
            reply = (CheckBox) itemView.findViewById(R.id.video_reply_count);
            mSurfaceView = (SurfaceView) itemView.findViewById(R.id.video_play);
            cover = (ImageView) itemView.findViewById(R.id.video_cover);
            mSurfaceView.getHolder().addCallback(this);
            mSurfaceView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int visibility = btnplay2.getVisibility();
                    if (visibility == View.INVISIBLE) {
                        btnplay2.setVisibility(View.VISIBLE);
                        sMediaPlayer.pause();
                        mPosition = sMediaPlayer.getCurrentPosition();
                    } else {
                        sMediaPlayer.start();
                        btnplay2.setVisibility(View.INVISIBLE);
                    }

                }
            });
            btnplay.setOnClickListener(this);
            SharedPreferences sharedPreferences = mContext.getSharedPreferences("FontSetting", Context.MODE_PRIVATE);
            float font1 = sharedPreferences.getFloat("Font1", 18);
            float font2 = sharedPreferences.getFloat("Font2", 20);
            videotitle.setTextSize(font1);
        }

        public void setItemPosition(int itemPosition) {
            this.itemPosition = itemPosition;
        }

        public void setInfo(String playUrl) {
            vplayUrl = playUrl;
        }

        @Override
        public void onClick(View v) {
            btnplay.setVisibility(View.GONE);
            cover.setVisibility(View.INVISIBLE);
            btnplay2.setVisibility(View.INVISIBLE);
            sMediaPlayer.reset();
            sMediaPlayer.setDisplay(mSurfaceView.getHolder());

            try {
                String url = vplayUrl; 
                if (url != null) {
                    sMediaPlayer.setDataSource(url);
                    sMediaPlayer.prepareAsync();
                    sPlayingPosition = itemPosition;
                    sMediaPlayer.setOnCompletionListener(this);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void surfaceCreated(SurfaceHolder holder) {

        }

        @Override
        public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

        }

        @Override
        public void surfaceDestroyed(SurfaceHolder holder) {

        }

        @Override
        public void onCompletion(MediaPlayer mp) {
            btnplay.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onCompletion(MediaPlayer mp) {

    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        mp.start();
    }
}
