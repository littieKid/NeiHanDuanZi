package com.neihanduanzi.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.neihanduanzi.R;
import com.neihanduanzi.model.AReply;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by 齐泽威 on 2016/11/1.
 */

public class PingLunAdapter extends BaseAdapter {
    private Context mContext;
    private List<AReply.Reply> mReplies;
    private LayoutInflater mInflater;

    public PingLunAdapter(List<AReply.Reply> replies, Context context) {
        mReplies = replies;
        mContext = context;
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return mReplies.size();
    }

    @Override
    public Object getItem(int position) {
        return mReplies.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.pinglun_item,parent,false);
            convertView.setTag(new ViewHolder(convertView));
        }
        ViewHolder holder = (ViewHolder) convertView.getTag();
        AReply.Reply reply = mReplies.get(position);
        SharedPreferences sharedPreferences = mContext.getSharedPreferences("FontSetting", Context.MODE_PRIVATE);
        float font1 = sharedPreferences.getFloat("Font1", 18);
        float font2 = sharedPreferences.getFloat("Font2", 20);

        holder.text.setTextSize(font1);

        holder.text.setText(reply.getText());
        holder.username.setText(reply.getUser_name());
        holder.dianzanCount.setText(String.valueOf(reply.getDigg_count()));
        Picasso.with(mContext).load(reply.getAvatar_url()).into(holder.userIcon);
        return convertView;
    }
    
    public class ViewHolder{
        private TextView username;
        private TextView dianzanCount;
        private TextView text;
        private ImageView userIcon;
        public ViewHolder(View view){
            username = (TextView)view.findViewById(R.id.user_name); 
            dianzanCount = (TextView)view.findViewById(R.id.dianzan_count); 
            text = (TextView)view.findViewById(R.id.pinglun_text); 
            userIcon = (ImageView)view.findViewById(R.id.user_icon); 
        }
    }
}
