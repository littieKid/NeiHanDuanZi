package com.neihanduanzi.animation;

import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;

import com.nineoldandroids.view.ViewHelper;

public class  RotateDownPageTransformer implements ViewPager.PageTransformer
{

    private static final float ROT_MAX = 20.0f;
    private float mRot;



    public void transformPage(View view, float position)
    {

        Log.e("TAG", view + " , " + position + "");

        if (position < -1)
        {
            ViewHelper.setRotation(view, 0);

        } else if (position <= 1)
        { // [-1,1]  这个页面还在屏幕右边缘即将滑入我们的视线
            if (position < 0)
            {

                mRot = (ROT_MAX * position);
                ViewHelper.setPivotX(view, view.getMeasuredWidth() * 0.5f);
                ViewHelper.setPivotY(view, view.getMeasuredHeight());
                ViewHelper.setRotation(view, mRot);
            } else
            {

                mRot = (ROT_MAX * position);
                ViewHelper.setPivotX(view, view.getMeasuredWidth() * 0.5f);
                ViewHelper.setPivotY(view, view.getMeasuredHeight());
                ViewHelper.setRotation(view, mRot);
            }



        } else
        {
            ViewHelper.setRotation(view, 0);
        }

    }
}