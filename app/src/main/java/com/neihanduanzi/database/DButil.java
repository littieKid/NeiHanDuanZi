package com.neihanduanzi.database;

import org.xutils.DbManager;
import org.xutils.ex.DbException;
import org.xutils.x;

import java.util.List;

/**
 * Created by 凸显 on 2016/11/3.
 */
public final class DButil {

    private DButil(){}

    private static DbManager manager;

    static  {
        // 配置数据库参数
        DbManager.DaoConfig config = new DbManager.DaoConfig();
        config.setDbName("neihan.db"); //   数据库名称
        config.setDbVersion(1);//  版本
        manager = x.getDb(config);// 获取数据库管理者
    }


    //  添加
    public static void addData(RecommendDb recommendBean) {
        try {
            manager.save(recommendBean);
        } catch (DbException e) {
            e.printStackTrace();
        }
    }

    public static void addData(DuanziDb recommendBean) {
        try {
            manager.save(recommendBean);
        } catch (DbException e) {
            e.printStackTrace();
        }
    }

    public  static void addData(ImageDb recommendBean) {
        try {
            manager.save(recommendBean);
        } catch (DbException e) {
            e.printStackTrace();
        }
    }

    public static void addData(VideoDb recommendBean) {
        try {
            manager.save(recommendBean);
        } catch (DbException e) {
            e.printStackTrace();
        }
    }


    // 全部删除
    public static void deleteAll() {
        try {
            manager.delete(RecommendDb.class);
            manager.delete(DuanziDb.class);
            manager.delete(VideoDb.class);
            manager.delete(ImageDb.class);
        } catch (DbException e) {
            e.printStackTrace();
        }
    }

    public static void deleteRecommend() {
        try {
            manager.delete(RecommendDb.class);
        } catch (DbException e) {
            e.printStackTrace();
        }
    }

    public static void deleteDuanzi() {
        try {
            manager.delete(DuanziDb.class);
        } catch (DbException e) {
            e.printStackTrace();
        }
    }

    public static void deleteImage() {
        try {
            manager.delete(ImageDb.class);
        } catch (DbException e) {
            e.printStackTrace();
        }
    }

    public static void deleteVideo() {
        try {
            manager.delete(VideoDb.class);
        } catch (DbException e) {
            e.printStackTrace();
        }
    }


    // 查询
    public  static List<RecommendDb> queryAllRecommend() {
        try {
            List<RecommendDb> all = manager.findAll(RecommendDb.class);
            return all;
        } catch (DbException e) {
            e.printStackTrace();
        }
        return null;
    }
    public static List<ImageDb> queryAllImage() {
        try {
            List<ImageDb> all = manager.findAll(ImageDb.class);
            return all;
        } catch (DbException e) {
            e.printStackTrace();
        }
        return null;
    }
    public static List<VideoDb> queryAllVideo() {
        try {
            List<VideoDb> all = manager.findAll(VideoDb.class);
            return all;
        } catch (DbException e) {
            e.printStackTrace();
        }
        return null;
    }
    public static List<DuanziDb> queryAllDuanzi() {
        try {
            List<DuanziDb> all = manager.findAll(DuanziDb.class);
            return all;
        } catch (DbException e) {
            e.printStackTrace();
        }
        return null;
    }
}