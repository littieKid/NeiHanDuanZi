package com.neihanduanzi.database;

import org.xutils.db.annotation.Column;
import org.xutils.db.annotation.Table;

/**
 * Created by 凸显 on 2016/11/4.
 */
@Table(name = "recommend")
public class RecommendDb {

    @Column(name = "id",isId = true,autoGen = true)
    private int id;
    @Column(name = "recommendGroup" )
    private String recommendGroup;

    public String getRecommendGroup() {
        return recommendGroup;
    }

    public void setRecommendGroup(String recommendGroup) {
        this.recommendGroup = recommendGroup;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
