package com.neihanduanzi;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import com.neihanduanzi.fragment.PingLunFragment;
import com.neihanduanzi.model.DuanZi;
import com.neihanduanzi.model.ImageModel;
import com.neihanduanzi.model.VideoModel;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import cn.sharesdk.framework.ShareSDK;
import cn.sharesdk.onekeyshare.OnekeyShare;

public class BaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);
        EventBus.getDefault().register(this);

        //0,段子 1,图片  2,视频
        int type = getIntent().getIntExtra("type", -1);
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        switch (type) {
            case 2:
                transaction.add(R.id.detail_fragment,new VideoDeialFragment());
                break;
            case 3:
                transaction.add(R.id.detail_fragment,new ImageFragment());
                break;
            case 4:
                transaction.add(R.id.detail_fragment, new DuanZiFragment());
                break;
        }
        transaction.add(R.id.detail_fragment, new PingLunFragment());
        transaction.commit();
    }

    @Override
    protected void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    @Subscribe
    public void Share(VideoModel.Fdata.VideoDetial fdata){
        ShareSDK.initSDK(this);
        OnekeyShare oks = new OnekeyShare();
        //关闭sso授权
        oks.disableSSOWhenAuthorize();

        // 分享时Notification的图标和文字  2.5.9以后的版本不调用此方法
        //oks.setNotification(R.drawable.ic_launcher, getString(R.string.app_name));
        // title标题，印象笔记、邮箱、信息、微信、人人网和QQ空间使用
        if (fdata.getTitle() != null) {
            oks.setTitle(fdata.getTitle());
        }else {
            oks.setTitle("标题");
        }
        // titleUrl是标题的网络链接，仅在人人网和QQ空间使用
//        if (titleUrl != null){
//            oks.setTitleUrl(titleUrl);
//        }else {
//            oks.setTitleUrl("http://www.baidu.com");
//        }
        // text是分享文本，所有平台都需要这个字段
        if (fdata.getTitle() != null){
            oks.setText(fdata.getTitle());
        }else {
            oks.setText("分享文本");
        }
        //分享网络图片，新浪微博分享网络图片需要通过审核后申请高级写入接口，否则请注释掉测试新浪微博
        String url = fdata.getCover().getUrl_list().get(0).getUrl();
        if (url != null) {
            oks.setImageUrl(url);
        }else {
            oks.setImageUrl("http://f1.sharesdk.cn/imgs/2014/02/26/owWpLZo_638x960.jpg");
        }
        // imagePath是图片的本地路径，Linked-In以外的平台都支持此参数
        //oks.setImagePath("/sdcard/test.jpg");//确保SDcard下面存在此张图片
        // url仅在微信（包括好友和朋友圈）中使用
//        if (weixinUrl != null){
//            oks.setUrl(weixinUrl);
//        }else {
//            oks.setUrl("http://sharesdk.cn");
//        }
// 启动分享GUI
        oks.show(this);
    }

    @Subscribe
    public void Share(DuanZi.Data.Group fdata){
        ShareSDK.initSDK(this);
        OnekeyShare oks = new OnekeyShare();
        oks.disableSSOWhenAuthorize();
        if (fdata.getContent()!= null) {
            oks.setTitle(fdata.getContent());
        }else {
            oks.setTitle("标题");
        }
        if (fdata.getContent() != null){
            oks.setText(fdata.getContent());
        }else {
            oks.setText("分享文本");
        }
        oks.show(this);
    }
    @Subscribe
    public void Share(ImageModel.Data.Group fdata){
        ShareSDK.initSDK(this);
        OnekeyShare oks = new OnekeyShare();
        oks.disableSSOWhenAuthorize();
        if (fdata.getContent()!= null) {
            oks.setTitle(fdata.getContent());
        }else {
            oks.setTitle("标题");
        }
        if (fdata.getContent() != null){
            oks.setText(fdata.getContent());
        }else {
            oks.setText("分享文本");
        }
        String url = fdata.getLarge_image().getUrl_list().get(0).getUrl();
        if (url != null) {
            oks.setImageUrl(url);
        }else {
            oks.setImageUrl("http://f1.sharesdk.cn/imgs/2014/02/26/owWpLZo_638x960.jpg");
        }
        oks.show(this);
    }
    
}
